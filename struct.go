package squalus

import (
	"fmt"
	"reflect"
	"strings"
)

// FieldNameConverter is for objects that define how to convert from struct field names to database names.
type FieldNameConverter interface {
	DBName(field string) string
}

// getAllFieldsRec is a helper for getAllFields, defined because Go has some limitations on recursive functions.
func getAllFieldsRec(val reflect.Value, fields *map[string]interface{}, prefix string) {
	typ := val.Type()

	// check whether the given type implements the interface to convert field names
	convert := typ.Implements(reflect.TypeOf((*FieldNameConverter)(nil)).Elem())
	var convertMethod reflect.Value
	if convert {
		convertMethod = val.MethodByName("DBName")
	}

	for i := 0; i < typ.NumField(); i++ {
		field := val.Field(i)
		fieldType := typ.Field(i)
		if field.Kind() == reflect.Struct && !scanDirect(field.Type()) {
			dbName := fieldType.Name
			tag, ok := getFieldTag(fieldType, "db")
			switch {
			case ok:
				dbName = tag
			case convert:
				res := convertMethod.Call([]reflect.Value{reflect.ValueOf(fieldType.Name)})
				if s, ok := res[0].Interface().(string); ok {
					dbName = s
				}
			}
			getAllFieldsRec(field, fields, prefix+dbName+".")
			if fieldType.Anonymous {
				// an "anonymous" struct results in two names for the same thing.
				getAllFieldsRec(field, fields, prefix)
			}
		} else {
			key := fieldType.Name
			tag, ok := getFieldTag(fieldType, "db")
			if ok {
				key = tag
			}
			switch {
			case ok:
				key = tag
			case convert:
				res := convertMethod.Call([]reflect.Value{reflect.ValueOf(fieldType.Name)})
				if s, ok := res[0].Interface().(string); ok {
					key = s
				}
			}
			(*fields)[prefix+key] = field.Addr().Interface()
		}
	}
}

// getFieldTag ignoring tags parameters separated by comma (ie. tag `db:"name,param1,.."` will return "name")
func getFieldTag(fieldType reflect.StructField, tagName string) (string, bool) {
	tag, ok := fieldType.Tag.Lookup(tagName)
	tag = strings.Split(tag, ",")[0]
	return tag, ok
}

// getAllFields returns all the fields of a struct, including those in embedded anonymous structs.
func getAllFields(val reflect.Value) map[string]interface{} {
	fields := map[string]interface{}{} // field name => pointer to field
	getAllFieldsRec(val, &fields, "")
	return fields
}

// destsForStruct returns the destinations, suitable for Scan, for a given struct value.
func destsForStruct(toVal reflect.Value, columns []string) ([]interface{}, error) {
	fields := getAllFields(toVal)

	dests := []interface{}{}
	for _, column := range columns {
		f, ok := fields[column]
		if !ok {
			return nil, fmt.Errorf("column %v has no corresponding field in struct %v", column, toVal.Type().Name())
		}
		dests = append(dests, f)
	}
	return dests, nil
}
