package squalus

type mssqlDriver struct{}

// NewMssqlDriver creates a `Driver` for mssql databases.
// It is meant to be used with `NewDBWithDriver`
func NewMssqlDriver() Driver {
	return mssqlDriver{}
}

func (d mssqlDriver) AdaptQuery(query string, params map[string]interface{}) (string, []interface{}, error) {
	return adaptQueryNumbered(query, params, "@p")
}
