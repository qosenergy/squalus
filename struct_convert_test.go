package squalus

import (
	"context"
	"database/sql"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type PersonWithFieldNameConverter struct {
	ID        int
	Name      string
	Height    float64
	BirthDate time.Time `db:"birth"` // this field still has a tag to check that this case is taken into account
}

func (PersonWithFieldNameConverter) DBName(field string) string {
	return strings.ToLower(field)
}

func QueryStructConverter(t *testing.T, db DB, _ *sql.DB) {
	clarissaCooper := PersonWithFieldNameConverter{
		ID:        3,
		Name:      "Clarissa Cooper",
		Height:    1.68,
		BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
	}
	var person PersonWithFieldNameConverter
	if err := db.Query(
		context.Background(),
		"select [name], [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person, "Person data")
}

type HeightConvert struct {
	Height float64
}

func (HeightConvert) DBName(field string) string {
	return strings.ToLower(field)
}

type NameBirthHeightConvert struct {
	Name      string
	BirthDate time.Time `db:"birth"`
	HeightConvert
}

func (NameBirthHeightConvert) DBName(field string) string {
	switch field {
	case "Name":
		return "name"
	case "HeightConvert":
		return "Height"
	default:
		panic("Invalid field name")
	}
}

type PersonEmbedConvert struct {
	ID int
	NameBirthHeightConvert
}

func (PersonEmbedConvert) DBName(field string) string {
	switch field {
	case "ID":
		return "id"
	case "NameBirthHeightConvert":
		return "name_birth_height"
	default:
		panic("Invalid field name")
	}
}

func QueryStructEmbeddedConverter(t *testing.T, db DB, _ *sql.DB) {
	clarissaCooper := PersonEmbedConvert{
		ID: 3,
		NameBirthHeightConvert: NameBirthHeightConvert{
			Name:      "Clarissa Cooper",
			BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
			HeightConvert: HeightConvert{
				Height: 1.68,
			},
		},
	}
	var person1 PersonEmbedConvert
	if err := db.Query(
		context.Background(),
		"select [name], [id], [birth], [height] from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person1,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person1, "Person data")

	var person2 PersonEmbedConvert
	if err := db.Query(
		context.Background(),
		"select [name] as [name_birth_height.name], [id], [birth], [height] as [name_birth_height.height]"+
			" from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person2,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person2, "Person data")
	var person3 PersonEmbedConvert
	if err := db.Query(
		context.Background(),
		"select [name] as [name_birth_height.name], [id], [birth], [height] as [name_birth_height.Height.height]"+
			" from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person3,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person3, "Person data")
}

type NameBirthHeightComposeConvert struct {
	Name      string        `db:"name"`
	BirthDate time.Time     `db:"birth"`
	H         HeightConvert `db:"hh"`
}

func (NameBirthHeightComposeConvert) DBName(field string) string {
	switch field {
	case "Name":
		return "name"
	case "BirthDate":
		return "birth"
	case "H":
		return "hh"
	default:
		panic("Invalid field name")
	}
}

type PersonComposedConvert struct {
	ID  int `db:"id"`
	NBH NameBirthHeightComposeConvert
}

func QueryStructComposedConverter(t *testing.T, db DB, _ *sql.DB) {
	clarissaCooper := PersonComposedConvert{
		ID: 3,
		NBH: NameBirthHeightComposeConvert{
			Name:      "Clarissa Cooper",
			BirthDate: time.Date(2003, 9, 30, 0, 0, 0, 0, time.UTC),
			H: HeightConvert{
				Height: 1.68,
			},
		},
	}

	var person1 PersonComposedConvert
	if err := db.Query(
		context.Background(),
		"select [name] as [NBH.name], [id], [birth] as [NBH.birth], [height] as [NBH.hh.height]"+
			" from [persons] where [id]={id}",
		map[string]interface{}{"id": 3},
		&person1,
	); err != nil {
		t.Fatalf("Failed to read person: %s", err)
	}
	assert.Equal(t, clarissaCooper, person1, "Person data")
}
