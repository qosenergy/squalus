package squalus

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReplace(t *testing.T) {
	t1 := "[t1]"
	t2 := "[\\[t2\\]]"
	t3 := "\\[[\\[t3\\]]\\]"

	assert.Equal(t, replaceSquareBracketsWithDoubleQuotes(t1), `"t1"`)
	assert.Equal(t, replaceSquareBracketsWithDoubleQuotes(t2), `"[t2]"`)
	assert.Equal(t, replaceSquareBracketsWithDoubleQuotes(t3), `["[t3]"]`)

	assert.Equal(t, replaceSquareBracketsWithBackticks(t1), "`t1`")
	assert.Equal(t, replaceSquareBracketsWithBackticks(t2), "`[t2]`")
	assert.Equal(t, replaceSquareBracketsWithBackticks(t3), "[`[t3]`]")
}
