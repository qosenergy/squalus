package squalus

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

// Driver is used by `squalus.DB` to reformat generic queries to a format complient with the corresponding DB.
// It is meant to be used to create a new DB with `NewDBWithDriver` and not as a standalone.
// in most case: NewDB will determine which drive to use automatically.
// `NewMySQLDriver`, `NewMsSqlDriver`, `NewSqliteDriver` and `NewPostgreSqlDriver` are
// exposed to create instances of the interface for the most used DBs.
type Driver interface {
	AdaptQuery(query string, params map[string]interface{}) (string, []interface{}, error)
}

var paramsRegexp = regexp.MustCompile("{([a-zA-Z0-9_]+)}")

type paramsInfos struct {
	value  interface{}
	length int
}

func getParamsInfos(params map[string]interface{}) map[string]paramsInfos {
	infos := make(map[string]paramsInfos)
	for name, value := range params {
		i := paramsInfos{value: value, length: 1}
		t := reflect.TypeOf(value)
		if t.Kind() == reflect.Slice && t.Elem().Kind() != reflect.Uint8 {
			i.length = reflect.ValueOf(value).Len()
			if i.length == 1 {
				// replace value with the value of the only element in the slice.
				i.value = reflect.ValueOf(value).Index(0).Interface()
			}
		}
		infos["{"+name+"}"] = i
	}
	return infos
}

// adaptQueryNumbered is a generic query adapter for numbered parameters.
func adaptQueryNumbered(query string, params map[string]interface{}, prefix string) (string, []interface{}, error) {
	infos := getParamsInfos(params)
	var adArgs []interface{}
	re := paramsRegexp

	// replace {args} with the appropriate <prefix>[n]
	rank := 1
	ranks := map[string]int{}
	missing := ""
	adQuery := re.ReplaceAllStringFunc(
		query,
		func(name string) string {
			i, ok := infos[name]
			if !ok {
				if missing == "" {
					missing = name
				}
				return ""
			}
			r, already := ranks[name]
			if !already {
				r = rank
				ranks[name] = rank
				rank++
			}
			res := fmt.Sprintf("%s%v", prefix, r)
			switch i.length {
			case 0:
				res = ""
			case 1:
				if !already {
					adArgs = append(adArgs, i.value)
				}
			default:
				for j := 0; j < i.length; j++ {
					if j != 0 {
						res += fmt.Sprintf(",%s%v", prefix, r+j)
					}
					if !already {
						adArgs = append(adArgs, reflect.ValueOf(i.value).Index(j).Interface())
					}
				}
			}
			rank = len(adArgs) + 1
			return res
		},
	)

	if missing != "" {
		return "", nil, fmt.Errorf("parameter %s was not given a value", missing)
	}

	return adQuery, adArgs, nil
}

// replaceSquareBracketsWithDoubleQuotes replaces [] with "" in a string
func replaceSquareBracketsWithDoubleQuotes(query string) string {
	return strings.NewReplacer("\\[", "[", "\\]", "]", `[`, `"`, `]`, `"`).Replace(query)
}

// replaceSquareBracketsWithBackticks replaces [] with `` in a string
func replaceSquareBracketsWithBackticks(query string) string {
	return strings.NewReplacer("\\[", "[", "\\]", "]", "[", "`", "]", "`").Replace(query)
}
