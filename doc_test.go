package squalus

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"
)

func ExampleSQLLink_exec() {
	db1, _ := sql.Open("", "")
	db, _ := NewDB(db1)
	defer func() { _ = db.Close() }()
	ctx := context.Background()
	_, _ = db.Exec(ctx, "CREATE TABLE [persons]([id] INT, [name] VARCHAR(128), [height] FLOAT, [birth] DATETIME)", nil)
	_, err := db.Exec(
		ctx,
		"INSERT INTO [persons]([id], [name], [height], [birth]) VALUES({id}, {name}, {height}, {birth})",
		map[string]interface{}{
			"id":     1,
			"name":   "Alice Abbott",
			"height": 1.65,
			"birth":  time.Date(1985, 7, 12, 0, 0, 0, 0, time.UTC),
		},
	)
	if err != nil {
		fmt.Printf("Got error %s", err)
	}
}

func ExampleSQLLink_query() {
	db1, _ := sql.Open("", "")
	db, _ := NewDB(db1)
	defer func() { _ = db.Close() }()
	ctx := context.Background()

	// Person represents a row in the persons table. Notice the db tags.
	type Person struct {
		ID        int       `db:"id"`
		Name      string    `db:"name"`
		Height    float64   `db:"height"` // in meters
		BirthDate time.Time `db:"birth"`
	}

	{
		// This is the most basic use of Query: the type of to is a basic type.
		var name string
		if err := db.Query(
			ctx,
			`SELECT [name] FROM [persons] WHERE [id]={id}`,
			map[string]interface{}{"id": 3},
			&name,
		); err != nil {
			fmt.Printf("Got error %s", err)
		}
		// name contains "Clarissa Cooper"
	}

	{
		// Here, the type of to is a struct that is automatically filled in using the db tags.
		var person Person
		if err := db.Query(
			ctx,
			`SELECT [name], [id], [birth], [height] FROM [persons] WHERE [id]={id}`,
			map[string]interface{}{"id": 3},
			&person,
		); err != nil {
			fmt.Printf("Got error %s", err)
		}
		// person contains Clarissa Cooper’s data.
	}

	{
		// The type of to can also be a slice, in which case it receives one item per row.
		var people []Person
		if err := db.Query(
			ctx,
			`SELECT [name], [id], [birth], [height] FROM [persons] ORDER BY [id]`,
			nil,
			&people,
		); err != nil {
			fmt.Printf("Got error %s", err)
		}
		// people contains all four persons
	}

	{
		// If the type of to is a channel, one item per row is sent into that channel.
		ch := make(chan Person)

		go func() {
			for p := range ch {
				fmt.Println(p)
			}
		}()

		if err := db.Query(
			ctx,
			`SELECT [name], [id], [birth], [height] FROM [persons] ORDER BY [id]`,
			nil,
			ch,
		); err != nil {
			fmt.Printf("Got error %s", err)
		}
		// all people are printed to stdout
	}

	{
		// If to is a function, it is called once for each row read from database.
		if err := db.Query(
			ctx,
			`SELECT [name], [id], [birth], [height] FROM [persons] ORDER BY [id]`,
			nil,
			func(name string, id int, birthDate time.Time, height float64) {
				fmt.Printf("%v has ID %v, birth date %v and height %v\n", name, id, birthDate, height)
			},
		); err != nil {
			fmt.Printf("Got error %s", err)
		}
	}

	{
		// Optionally, the callback may return an error. This stops the query execution returns that error.
		if err := db.Query(
			ctx,
			`SELECT [name], [id], [birth], [height] FROM [persons]`,
			nil,
			func(name string, id int, birthDate time.Time, height float64) error {
				if name == "Donald Dock" {
					return errors.New("found an intruder")
				}
				return nil
			},
		); err != nil {
			fmt.Printf("Got error %s", err)
		}
		// Query returns an error with message "found an intruder".
	}
}
